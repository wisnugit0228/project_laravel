<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h5>Sign Up From</h5>
    <form action="/welcome" method="get">
      <label for="fname">First Nama</label><br />
      <input type="text" id="fname" name="fname" />
      <br /><br />
      <label for="lname">Last Name</label><br />
      <input type="text " id="lname" name="lname"/>
      <br /><br />
      <label for=""> Gender</label><br />
      <input type="radio" />Male <br />
      <input type="radio" />Female <br />
      <input type="radio" />Other <br /><br />
      <label for="">Nationality</label><br />
      <select name="nation" id="">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="singapura">Singapura</option></select
      ><br /><br />
      <label for="">Language Spoken</label><br />
      <input type="checkbox" />Indonesia <br />
      <input type="checkbox" />Inggris <br />
      <input type="checkbox" />Other <br /><br />
      <label for="bio">Bio</label><br />
      <textarea name="" id="bio" cols="30" rows="10"></textarea><br /><br />
      <input type="submit" />
    </form>
  </body>
</html>
